import pygame as pg
import pygame_gui as gui


class ResetButton(gui.elements.UIButton):
    def __init__(self, relative_rect: pg.Rect, text: str, manager, simulation):
        super().__init__(relative_rect, text, manager)
        self.sim = simulation


class ConfigureButton(gui.elements.UIButton):
    def __init__(self, relative_rect: pg.Rect, text: str, manager):
        super().__init__(relative_rect, text, manager)
